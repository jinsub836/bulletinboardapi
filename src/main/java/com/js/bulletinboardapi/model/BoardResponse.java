package com.js.bulletinboardapi.model;

import com.js.bulletinboardapi.enums.BoardTag;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardResponse {
    private Long id;

    private LocalDate dateCreate;

    private String boardTag;

    private String writer;

    private String title;

    private Integer goods;

    private Integer inquiry;
}
