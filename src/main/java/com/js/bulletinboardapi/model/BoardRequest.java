package com.js.bulletinboardapi.model;

import com.js.bulletinboardapi.enums.BoardTag;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardRequest {

    private BoardTag boardTag;

    private String writer;

    private String title;

    private Integer goods;

    private Integer inquiry;

}
