package com.js.bulletinboardapi.entity;

import com.js.bulletinboardapi.enums.BoardTag;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
public class BulletinBoard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false , length = 15)
    private BoardTag boardTag;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false , length = 15)
    private String writer;

    @Column(nullable = false , length = 30)
    private String title;

    @Column(nullable = false)
    private Integer goods;

    @Column(nullable = false)
    private Integer inquiry;

}
