package com.js.bulletinboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BoardTag {
    LoL("롤")
    ,FIFA("피파")
    ,MAPLE_STORY("메이플 스토리")
    ,FM24("FM24");

    private final String game;
}
