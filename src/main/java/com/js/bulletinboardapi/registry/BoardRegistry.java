package com.js.bulletinboardapi.registry;

import com.js.bulletinboardapi.entity.BulletinBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRegistry extends JpaRepository<BulletinBoard, Long> {
}
