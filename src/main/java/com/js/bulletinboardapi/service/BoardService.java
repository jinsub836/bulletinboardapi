package com.js.bulletinboardapi.service;

import com.js.bulletinboardapi.entity.BulletinBoard;
import com.js.bulletinboardapi.model.BoardChangeRequest;
import com.js.bulletinboardapi.model.BoardItem;
import com.js.bulletinboardapi.model.BoardRequest;
import com.js.bulletinboardapi.model.BoardResponse;
import com.js.bulletinboardapi.registry.BoardRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRegistry boardRegistry;

    public void setBoard(BoardRequest boardRequest){
        BulletinBoard addData = new BulletinBoard();
        addData.setBoardTag(boardRequest.getBoardTag());
        addData.setDateCreate(LocalDate.now());
        addData.setWriter(boardRequest.getWriter());
        addData.setTitle(boardRequest.getTitle());
        addData.setGoods(boardRequest.getGoods());
        addData.setInquiry(boardRequest.getInquiry());

        boardRegistry.save(addData);
    }

    public List<BoardItem> getBoard(){
        List<BulletinBoard> originlist = boardRegistry.findAll();
        List<BoardItem> result = new LinkedList<>();

        for (BulletinBoard bulletinBoard : originlist){
        BoardItem addItem = new BoardItem();
        addItem.setId(bulletinBoard.getId());
        addItem.setDateCreate(bulletinBoard.getDateCreate());
        addItem.setWriter(bulletinBoard.getWriter());
        addItem.setTitle(bulletinBoard.getTitle());
        addItem.setGoods(bulletinBoard.getGoods());

        result.add(addItem);

    }  return result;}

    public BoardResponse getBoardResponse(long id){
        BulletinBoard originData = boardRegistry.findById(id).orElseThrow();
        BoardResponse response = new BoardResponse();
        response.setId(originData.getId());
        response.setDateCreate(originData.getDateCreate());
        response.setBoardTag(originData.getBoardTag().getGame());
        response.setWriter(originData.getWriter());
        response.setTitle(originData.getTitle());
        response.setGoods(originData.getGoods());
        response.setInquiry(originData.getInquiry());

        return response;
    }
    public void putChange (long id,BoardChangeRequest request){
        BulletinBoard originData = boardRegistry.findById(id).orElseThrow();

       originData.setBoardTag(request.getBoardTag());
       originData.setTitle(request.getTitle());

        boardRegistry.save(originData);
    }

    public void delBoard(long id){ boardRegistry.findById(id).orElseThrow();}
}
