package com.js.bulletinboardapi.controller;


import com.js.bulletinboardapi.model.BoardChangeRequest;
import com.js.bulletinboardapi.model.BoardItem;
import com.js.bulletinboardapi.model.BoardRequest;
import com.js.bulletinboardapi.model.BoardResponse;
import com.js.bulletinboardapi.service.BoardService;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;

    @PostMapping("/new")
    public String setBoard(@RequestBody BoardRequest boardRequest){
        boardService.setBoard(boardRequest);

        return "입력 완료";

    }

    @GetMapping("/list")
    public List<BoardItem> getBoard(){
        return boardService.getBoard();}

    @GetMapping("/detail/{id}")
    public BoardResponse getBoardResponse(@PathVariable long id){
       return boardService.getBoardResponse(id);
    }

    @PutMapping("title/{id}")
    public String putChange(@PathVariable long id, @RequestBody BoardChangeRequest boardChangeRequest){
      boardService.putChange(id,boardChangeRequest);
      return "수정 완료";
    }

    @DeleteMapping("/del/{id}")
    public String delBoard(@PathVariable long id){
        boardService.delBoard(id);
        return "삭제 완료";
    }
}
